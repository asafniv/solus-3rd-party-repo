Solus 3rd Party Repo
======

*These packages are not officials, so not reviewed or supported by the Solus team.*

## Adding the repo

`sudo eopkg add-repo devil505 https://gitlab.com/devil505/solus-3rd-party-repo/raw/master/packages/eopkg-index.xml.xz`
 
## Enabling the Repository

`sudo eopkg enable-repo devil505`

## Removing the Repository

`sudo eopkg remove-repo devil505`

## Disabling the Repository

`sudo eopkg disable-repo devil505`

## Want a package in Solus official repo ?

Make a package request at <http://dev.solus-project.com/> or support the opened task (see below)

If the Solus dev team is interested, I'll provide a patch to them.

## Packages list


| Package name| Task | Comment
| --- | --- | ---
| aesop |  | 
| audience |  | 
| budgie-brightness-applet | [TASK 5945](https://dev.solus-project.com/T5495) | 
| budgie-extra |  | 
| budgie-weather-applet | [TASK 4482](https://dev.solus-project.com/T4482) | 
| ciano |  | 
| duckietv | [TASK 1338](https://dev.solus-project.com/T1338) |  | 
| elementary-calendar |  | 
| elementary-calendar-devel |  | 
| eradio |  | 
| findfileconflicts |  | 
| gamehub |  | 
| gbml | REMOVED | NOW IN SOLUS REPO ! |
| geth |  | 
| gimp-lensfun |  | 
| gimp-plugin-beautify |  | 
| gimp-plugin-bimp |  | 
| imageburner |  | 
| karim |  | 
| megasync | [TASK 163](https://dev.solus-project.com/T163)  | rejected but they can change their opinions later |
| monilet |  | 
| nautilus-megasync |  | 
| news |  | 
| notejot |  | 
| nvidia-396-glx-driver-32bit | removed | ADDED TO OFFICIAL SOLUS REPO !|
| nvidia-396-glx-driver | removed  | ADDED TO OFFICIAL SOLUS REPO !|
| nvidia-396-glx-driver-common | removed  | ADDED TO OFFICIAL SOLUS REPO !| 
| nvidia-396-glx-driver-current | removed  | ADDED TO OFFICIAL SOLUS REPO !|
| nvidia-396-glx-driver-modaliases | removed  | ADDED TO OFFICIAL SOLUS REPO !|
| palaura |  | 
| playmymusic | [TASK 6683](https://dev.solus-project.com/T6683) | rejected... | 
| proton | Removed | useless since steam brings its own version |
| protonmail-bridge-bin | | |
| protonvpn-cli | REMOVED | NOW IN SOLUS REPO ! | 
| qarte |  | 
| qomui | [TASK 6722](https://dev.solus-project.com/T6722) | Patch submitted [D3760](https://dev.solus-project.com/D3760) | 
| quilter |  | 
| screencast |  | 
| showmypictures |  | 
| stacer | [TASK 5321](https://dev.solus-project.com/T5321) | 
| tutrl | [TASK 535](https://dev.solus-project.com/T535) |  | 
| wireguard | [TASK 3778](https://dev.solus-project.com/T3778) |  | 
| wireguard-current | [TASK 3778](https://dev.solus-project.com/T3778) |  | 
| wireguard-tools | [TASK 3778](https://dev.solus-project.com/T3778) |  | 

## A problem ?

Please fill a report [here](https://gitlab.com/devil505/solus-3rd-party-repo/issues)

Don't post on Solus forums, subreddit...etc. Solus dev team seems bored to heard about this repo XD
